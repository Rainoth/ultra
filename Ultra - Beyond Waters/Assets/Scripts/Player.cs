﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Being {
	
	public float runSpeed;
	[HideInInspector] public int facing;
	private Animator animator;
	[HideInInspector] public static Collider2D Collider_player;

	// Use this for initialization
	void Start () {
		x = gameObject.transform.position.x;
		y = gameObject.transform.position.y;
		Collider_player = GetComponent<BoxCollider2D>();
	}
	
	void FixedUpdate () {
		vel_x = axis_horizontal * runSpeed;

		transform.position = new Vector2(x + vel_x ,y);
		
		x = gameObject.transform.position.x;
		y = gameObject.transform.position.y;
		
		//RaycastHit2D terrain_right = Physics2D.Raycast(touchPoint.position, Vector2.right, 0.01f, groundLayers);
		//RaycastHit2D terrain_left = Physics2D.Raycast(touchPoint.position, Vector2.left, 0.01f, groundLayers);
		
		//x = gameObject.transform.position.x;
		//y = gameObject.transform.position.y;
	}
}
