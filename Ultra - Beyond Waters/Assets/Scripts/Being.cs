﻿using UnityEngine;
using System.Collections;

public class Being : MonoBehaviour {

	public float x, y, vel_x, vel_y;
	[HideInInspector] public bool key_right, key_left, key_down;
	[HideInInspector] public float axis_horizontal, axis_vertical;
	
	void Update () {
		key_down = (Input.GetKey(KeyCode.DownArrow));
		axis_horizontal = Input.GetAxis("Horizontal");
		axis_vertical = Input.GetAxis("Vertical");
		
		if (Input.GetKey("escape")) Application.Quit();
	}
	
}
